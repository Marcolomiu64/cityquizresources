package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.cityquiz.presentation.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("cat.itb.cityquiz", appContext.getPackageName());
    }

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void NavigationTest(){

        useAppContext();

        onView(withId(R.id.startButton)).perform(click());
        onView(withId(R.id.imageQuiz)).check(matches(isDisplayed()));
        onView(withId(R.id.materialButton1)).perform(click());
        onView(withId(R.id.imageQuiz)).check(matches(isDisplayed()));
        onView(withId(R.id.materialButton2)).perform(click());
        onView(withId(R.id.imageQuiz)).check(matches(isDisplayed()));
        onView(withId(R.id.materialButton3)).perform(click());
        onView(withId(R.id.imageQuiz)).check(matches(isDisplayed()));
        onView(withId(R.id.materialButton4)).perform(click());
        onView(withId(R.id.imageQuiz)).check(matches(isDisplayed()));
        onView(withId(R.id.materialButton5)).perform(click());
        onView(withId(R.id.textViewScore)).check(matches(isDisplayed()));
        onView(withId(R.id.againButton)).perform(click());
    }
}
