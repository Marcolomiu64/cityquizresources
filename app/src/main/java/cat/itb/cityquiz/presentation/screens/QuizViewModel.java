package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class QuizViewModel extends ViewModel {

    private Game game;
    private GameLogic gameLogic = RepositoriesFactory.getGameLogic();

    public void startQuiz() {

        game = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
    }

    public Game getGame() {
        return game;
    }

    public Game answerQuestion(int resposta){

        Game g = gameLogic.answerQuestions(game,resposta);
        return g;
    }
}
