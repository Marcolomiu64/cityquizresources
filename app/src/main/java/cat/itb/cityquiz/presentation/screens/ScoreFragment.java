package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class ScoreFragment extends Fragment {

    private QuizViewModel mViewModel;
    private Button button;
    private TextView ScoreFinal;

    public static ScoreFragment newInstance() {
        return new ScoreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.score_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button = view.findViewById(R.id.againButton);
        ScoreFinal = view.findViewById(R.id.textViewScore);
        button.setOnClickListener(this::startQuiz);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        Game game = mViewModel.getGame();
        String score = String.valueOf(game.getNumCorrectAnswers());
        ScoreFinal.setText(score);
    }

    private void startQuiz(View view) {
        Navigation.findNavController(view).navigate(R.id.action_score_to_quiz);
    }
}
