package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.List;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;

public class QuizFragment extends Fragment {

    private QuizViewModel mViewModel;
    Button b1, b2, b3, b4, b5 ,b6;
    ImageView image;

    public static QuizFragment newInstance() {
        return new QuizFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.quiz_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        mViewModel.startQuiz();

        Game game = mViewModel.getGame();
        display(game);

        /*String fileName = ImagesDownloader.scapeName(cityname);
        int resId = appContext.getResources().getIdentifier(fileName, "drawable", appContext.getPackageName());
        view.setImageResource(resId);*/
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        b1 = view.findViewById(R.id.materialButton1);
        b1.setOnClickListener(this::answeredQuestion);
        b2 =view.findViewById(R.id.materialButton2);
        b2.setOnClickListener(this::answeredQuestion);
        b3 = view.findViewById(R.id.materialButton3);
        b3.setOnClickListener(this::answeredQuestion);
        b4 = view.findViewById(R.id.materialButton4);
        b4.setOnClickListener(this::answeredQuestion);
        b5 = view.findViewById(R.id.materialButton5);
        b5.setOnClickListener(this::answeredQuestion);
        b6 = view.findViewById(R.id.materialButton6);
        b6.setOnClickListener(this::answeredQuestion);

        image = getView().findViewById(R.id.imageQuiz);
    }

    private void answeredQuestion(View view) {
        switch(view.getId()){
            case R.id.materialButton1:
                mViewModel.answerQuestion(0);
                break;
            case R.id.materialButton2:
                mViewModel.answerQuestion(1);
                break;
            case R.id.materialButton3:
                mViewModel.answerQuestion(2);
                break;
            case R.id.materialButton4:
                mViewModel.answerQuestion(3);
                break;
            case R.id.materialButton5:
                mViewModel.answerQuestion(4);
                break;
            case R.id.materialButton6:
                mViewModel.answerQuestion(5);
                break;
        }
        display(mViewModel.getGame());
    }

    private void display(Game game) {

        if(game.isFinished()){
            Navigation.findNavController(getView()).navigate(R.id.action_quiz_to_score);
        }else {


            List<City> cities = game.getCurrentQuestion().getPossibleCities();

            b1.setText(cities.get(0).getName());
            b2.setText(cities.get(1).getName());
            b3.setText(cities.get(2).getName());
            b4.setText(cities.get(3).getName());
            b5.setText(cities.get(4).getName());
            b6.setText(cities.get(5).getName());

            City correctCity = game.getCurrentQuestion().getCorrectCity();

            String fileName = ImagesDownloader.scapeName(correctCity.getName());
            int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
            image.setImageResource(resId);


            //game.questionAnswered();

            //game.getCurrentQuestion().getPossibleCities().get(0).getClass();

        }
    }

/*    private void getName() {
    }*/

}
